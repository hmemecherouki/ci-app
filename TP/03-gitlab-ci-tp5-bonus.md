# TP #4 - Debug d'un pipeline Gitlab-CI

> **Objectifs du TP**
> * Découvrir comment débugguer un pipeline pour réduire la boucle de feedback
> * Détecter facilement les erreurs de syntaxe avec le Linter intégré dans Gitlab
>
> **Prérequis**
> Aucun prérequis
>
> **Niveau de difficulté :** Débutant

Vous avez probablement déjà constaté qu'il est assez difficile d'écrire un nouveau job du premier coup sans erreur.
Nous vous avons dit qu'il fallait toujours chercher à réduire la boucle de feedback: debug le pipeline en local est un moyen d'y parvenir ici.
Plusieurs solutions existent, nous vous en proposons 2 ici:

# Vérification et correction de la syntaxe de votre fichier gitlab-ci.yml

Toutes les instances de Gitlab (Gitlab.com ou votre Gitlab instancié) proposent un outil pour vérifier la syntaxe de vos fichiers `.gitlab-ci.yml`

Pour trouver l'outil de vérification de syntaxe Gitlab-CI:
- aller sur un projet qui contient un pipeline
- cliquer sur le menu gauche "CI/CD" puis sur "Pipelines"
- le bouton CI-Lint permet d'afficher le Linter

![](./.assets/ci-lint.png)

Maintenant que vous êtes sur la page de vérification de la syntaxe, utilisez-la pour corriger la syntaxe du fichier suivant :

```yaml
variables:
  PYTHON3_IMAGE: python:alpine

stages:
  - syntax

PyCodeStyle:
  image: $PYTHON3_IMAGE
  stage: syntaxe
  script:
    - pip install pycodestyle
    - pycodestyle app.py

Pylint:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
 - pip install pylint
 - pylint app.py
```

> **Tips**: il va vous falloir adapter au moins 4 lignes

# Reproduction d'un pipeline en local

Comme présenté avec les slides, les runners gitlab peuvent être de plusieurs types (docker, local, ...). Nous allons installer ici directement
le binaire `gitlab-runner` sur l'environnement local afin de pouvoir l'utiliser pour tester notre pipeline.

Exécutez la procédure suivante pour l'installer sur Ubuntu :

```bash
$ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
$ sudo apt-get install gitlab-runner=12.6.0
```

Testez la bonne installation du binaire avec la commande suivante:

```
$ gitlab-runner
```

Vous devriez avoir un résultat similaire à celui-ci:

```
$ gitlab-runner
Runtime platform                                    arch=amd64 os=linux pid=23738 revision=ac8e767a version=12.6.0
NAME:
   gitlab-runner - a GitLab Runner

USAGE:
   gitlab-runner [global options] command [command options] [arguments...]

VERSION:
   12.6.0 (ac8e767a)

AUTHOR:
   GitLab Inc. <support@gitlab.com>

COMMANDS:
     exec                  execute a build locally
     list                  List all configured runners
     run                   run multi runner service
     register              register a new runner
     install               install service
     uninstall             uninstall service
     start                 start service
     stop                  stop service
     restart               restart service
     status                get status of a service
     run-single            start single runner
     unregister            unregister specific runner
     verify                verify all registered runners
     artifacts-downloader  download and extract build artifacts (internal)
     artifacts-uploader    create and upload build artifacts (internal)
     cache-archiver        create and upload cache artifacts (internal)
     cache-extractor       download and extract cache artifacts (internal)
     cache-init            changed permissions for cache paths (internal)
     health-check          check health for a specific address
     help, h               Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --cpuprofile value           write cpu profile to file [$CPU_PROFILE]
   --debug                      debug mode [$DEBUG]
   --log-format value           Choose log format (options: runner, text, json) [$LOG_FORMAT]
   --log-level value, -l value  Log level (options: debug, info, warn, error, fatal, panic) [$LOG_LEVEL]
   --help, -h                   show help
   --version, -v                print the version
```

Nous allons maintenant simuler un projet en créant plusieurs fichiers. Comme l'objectif de ce TP n'est pas de construire l'application, nous allons cloner un dépôt existant.

Clonez le dépôt `https://github.com/octo-technology/debug-pipeline` sur la branche `run-pipeline-locally`:

```bash
$ cd ~
$ git clone https://github.com/octo-technology/debug-pipeline.git
$ cd debug-pipeline
$ git checkout run-pipeline-locally
```

Votre arborescence doit ressembler à cela :

```bash
$ tree -a -I .git
.
├── .gitignore
├── .gitlab-ci.yml
├── app.js
├── package.json
└── sample.test.js

0 directories, 5 files
```

Le fichier `.gitlab-ci.yml` doit ressembler à cela:

```yaml
# .gitlab-ci.yml
image: node:latest

Build:
  stage: build
  script:
    - docker build .

Unit-Tests:
  stage: test
  script:
    - npm test
```

Exécutez maintenant le pipeline en local:

```bash
gitlab-runner exec docker Unit-Tests
```

Vous devriez avoir un résultat similaire à celui-ci :
```bash
ubuntu@gitlab-ci-training-yol-dev:~/test$ gitlab-runner exec docker Unit-Tests
Runtime platform                                    arch=amd64 os=linux pid=22294 revision=ac8e767a version=12.6.0
WARNING: You most probably have uncommitted changes.
WARNING: These changes will not be tested.
Running with gitlab-runner 12.6.0 (ac8e767a)
Using Docker executor with image node:latest ...
Pulling docker image node:latest ...
Using docker image sha256:f7756628c1ee047c3b9246fe4eea25880386c26ed46c5bc5af11fddc90e91771 for node:latest ...
Running on runner--project-0-concurrent-0 via gitlab-ci-training-yol-dev...
Fetching changes...
Initialized empty Git repository in /builds/project-0/.git/
Created fresh repository.
From /home/ubuntu/test
 * [new branch]      master     -> origin/master
Checking out feccd64d as master...

Skipping Git submodules setup
$ npm test

> sampleapp@0.0.1 test /builds/project-0
> jest

sh: 1: jest: not found
npm ERR! Test failed.  See above for more details.
ERROR: Job failed: exit code 1
FATAL: exit code 1
```

A notre grand regret, nous constatons ici que le job n'est pas correct. Il semblerait que le paquet npm `jest` ne soit pas installé.

> Question
>
> Corriger maintenant le fichier .gitlab-ci.yml pour faire fonctionner le job
> Tips : la commande `npm install` permet d'installer les dépendances npm qui sont référencées dans le fichier `package.json`

Après correction, vous devriez avoir un résultat similaire à celui-ci:
```bash
ubuntu@gitlab-ci-training-yol-dev:~/test$ gitlab-runner exec docker Unit-Tests
Runtime platform                                    arch=amd64 os=linux pid=20680 revision=ac8e767a version=12.6.0
Running with gitlab-runner 12.6.0 (ac8e767a)
Using Docker executor with image node:latest ...
Pulling docker image node:latest ...
Using docker image sha256:f7756628c1ee047c3b9246fe4eea25880386c26ed46c5bc5af11fddc90e91771 for node:latest ...
Running on runner--project-0-concurrent-0 via gitlab-ci-training-yol-dev...
Fetching changes...
Initialized empty Git repository in /builds/project-0/.git/
Created fresh repository.
From /home/ubuntu/test
 * [new branch]      master     -> origin/master
Checking out feccd64d as master...

Skipping Git submodules setup
$ npm install
npm WARN deprecated superagent@3.8.3: Please note that v5.0.1+ of superagent removes User-Agent header by default, therefore you may need to add it yourself (e.g. GitHub blocks requests without a User-Agent header).  This notice will go away with v5.0.2+ once it is released.
npm WARN deprecated left-pad@1.3.0: use String.prototype.padStart()
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN sampleapp@0.0.1 No repository field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.11 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.11: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

added 509 packages from 384 contributors and audited 877738 packages in 19.41s

15 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

$ npm test

> sampleapp@0.0.1 test /builds/project-0
> jest

PASS ./sample.test.js
  Sample Test
    ✓ should test that true === true (3ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        0.788s
Ran all test suites.
Job succeeded
```

Fin du TP.
