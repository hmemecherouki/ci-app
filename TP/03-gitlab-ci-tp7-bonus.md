# TP #6 - Installation de gitlab-runner
> **Objectifs du TP**
> * Installer Gitlab-Runner via le dépôt officiel de la distribution
> * Enregistrer le runner
>
> **Prérequis**
> Avant de commencer ce TP, vous devez avoir satisfait les prérequis suivants
> * Une machine avec un navigateur
> * Un accès à internet
> **Niveau de difficulté :** Débutant


## Installer Gitlab-Runner via le dépôt officiel de la distribution

Ajouter le repository gitlab-runner :

```bash
$ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
```

Installer le package gitlab-runner :
```bash
$ sudo apt-get install gitlab-runner
```

## Enregistrer un specific runner

Dans le projet “ci-app”, cliquer sur “Settings > CI/CD > Runners” pour afficher le token :

![](./.assets/register-runner.png)

Exécuter :
```bash
$ sudo gitlab-runner register
```
Saisir l’url de gitlab :
```bash
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/
```
Saisir le token récuperer dans l'interface gitlab :  
```bash
Please enter the gitlab-ci token for this runner:

```
Saisir “Specific runner of ci-app project” pour la description du runner :
```bash
Please enter the gitlab-ci description for this runner:
[ip-172-31-1-237]: Specific runner of ci-app project
```

Saisir “” pour les tags du runner :
```bash
Please enter the gitlab-ci tags for this runner (comma separated):

```
Saisir **docker** pour l’exécuteur :

```bash
Please enter the executor: shell, virtualbox, docker-ssh+machine, custom, docker, docker-ssh, kubernetes, parallels, ssh, docker+machine:
docker
```

Saisir **centos7** pour l’image docker à utiliser par l’exécuteur :
```bash
Please enter the default Docker image (e.g. ruby:2.6):
centos7
```

Vérifier que le runner est enregistré :	
 
- Ce message doit s’afficher :
```bash
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```
- Dans le projet “test”, cliquer sur “Settings > CI/CD > Runners”, le runner doit apparaître dans la liste des runners actifs du projet :
![](./.assets/registered-runner.png)



